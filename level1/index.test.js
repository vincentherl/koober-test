const index = require('./index.js');

test('I should get a total price of one cart', () => {
  input_articles = [
    {
      "id": 1,
      "name": "water",
      "price": 100
    },
    {
      "id": 2,
      "name": "honey",
      "price": 200
    }];

  input_items = [
    {
      "article_id": 1,
      "quantity": 6
    },
    {
      "article_id": 2,
      "quantity": 2
    }
  ];
  
  expect(index.getCartTotal(
    input_items,
    input_articles)).toBe(1000);
});

test('Get all resume carts', () => {
  var input_carts = [{
      "id": 1,
      "items": [
        {
          "article_id": 1,
          "quantity": 6
        },
        {
          "article_id": 2,
          "quantity": 2
        },
        {
          "article_id": 4,
          "quantity": 1
        }
      ]
    },
    {
      "id": 2,
      "items": [
        {
          "article_id": 2,
          "quantity": 1
        },
        {
          "article_id": 3,
          "quantity": 3
        }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ];

  var input_articles = [
    {
      "id": 1,
      "name": "water",
      "price": 100
    },
    {
      "id": 2,
      "name": "honey",
      "price": 200
    },
    {
      "id": 3,
      "name": "mango",
      "price": 400
    },
    {
      "id": 4,
      "name": "tea",
      "price": 1000
    }
  ];
  
  expect(index.getResumeCards(input_carts,input_articles)).toEqual(
    [
      {
        "id": 1,
        "total": 2000
      },
      {
        "id": 2,
        "total": 1400
      },
      {
        "id": 3,
        "total": 0
      }
    ]
  );
});