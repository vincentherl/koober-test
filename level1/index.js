const fs = require('fs');

/**
 * @param items
 * @param articles 
 * @returns {number}
 */
function getCartTotal(items,articles) {
  var total = 0;
  for (var item of items) {
    total = total + articles.find(article => article.id === item.article_id).price * item.quantity;
  }
  return total;
}

/**
 * @param carts
 * @param articles
 * @returns {Array}
 */
function getResumeCards(carts,articles) {
  var cartsTotal = [];
  for (var cart of carts) {
    cartsTotal.push({
      "id":cart.id,
      "total":getCartTotal(cart.items,articles)
    });
  }
  return cartsTotal;
}

function main() {
  fs.readFile('./input.json',{encoding:'utf-8',flag:'r'}, (err,data) => {
    if (err) throw err;
    input = JSON.parse(data);
    
    output = getResumeCards(input.carts,input.articles);
    
    fs.writeFile('./output.json', JSON.stringify(output), (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
  });
}

module.exports = {
  main:main,
  getCartTotal:getCartTotal,
  getResumeCards:getResumeCards
};

if (require.main === module) {
  main();
}
