const index = require('./index.js');

test('I should get articles with apply percent discount',() => {
  var inputArticle = {
    "id": 5,
    "name": "ketchup",
    "price": 999
  };
  
  var intputDiscountPercent = {
      "article_id": 5,
      "type": "percentage",
      "value": 30
  };

  var expectedArticle = {
    "id": 5,
    "name": "ketchup",
    "price": 699
  };
  
  expect(index.applyPercent(inputArticle,intputDiscountPercent)).toEqual(expectedArticle);

  var intputDiscountOther= {
    "article_id": 5,
    "type": "other",
    "value": 30
  };

  expect(index.applyPercent(inputArticle,intputDiscountOther)).toEqual(inputArticle);

});

test('I should get articles with apply amount discount',() => {
  var inputArticle = {
    "id": 2,
    "name": "honey",
    "price": 200
  };

  var inputDiscountAmount = {
    "article_id": 2,
    "type": "amount",
    "value": 25
  };

  var expectedArticle = {
    "id": 2,
    "name": "honey",
    "price": 175
  };
  
  expect(index.applyAmount(inputArticle,inputDiscountAmount)).toEqual(expectedArticle);
  
  var inputDiscountOther = {
    "article_id": 2,
    "type": "other",
    "value": 25
  };

  expect(index.applyAmount(inputArticle,inputDiscountOther)).toEqual(inputArticle);
});


test("I should apply dicount on articles", () => {
  var inputArticles = [{
    "id": 5,
    "name": "ketchup",
    "price": 999
  },{
    "id": 2,
    "name": "honey",
    "price": 200
  }];

  var inputDiscounts = [{
    "article_id": 5,
    "type": "percentage",
    "value": 30
  },{
    "article_id": 2,
    "type": "amount",
    "value": 25
  }];
  
  
  var expectedDiscount = [{
    "id": 5,
    "name": "ketchup",
    "price": 699
  },{
    "id": 2,
    "name": "honey",
    "price": 175
  }];
  
  expect(index.applyDiscounts(inputArticles,inputDiscounts)).toEqual(expectedDiscount);
  
});