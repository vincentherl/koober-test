const lvl1_index = require('../level1/index.js');
const lvl2_index = require('../level2/index.js');
const fs = require('fs');

/**
 * @param article
 * @param discount
 * @returns {*}
 */
function applyPercent(article,discount) {
  if (discount.type !== "percentage") {
    return article;
  }
  article.price -= Math.ceil(article.price * (discount.value / 100));
  return article;
}

/**
 * @param article
 * @param discount
 * @returns {*}
 */
function applyAmount(article,discount) {
  if (discount.type !== "amount") {
    return article;
  }
  article.price -= discount.value;
  return article;
}

/**
 * @param articles
 * @param discounts
 * @returns {Uint8Array | any[] | Int32Array | Uint16Array | Uint32Array | Float64Array | any}
 */
function applyDiscounts(articles,discounts) {
  return articles.map(article => {
    let discount = discounts.find(discount => discount.article_id === article.id);
    if (discount === undefined) {
      return article;
    }
    article = applyAmount(article,discount);
    article = applyPercent(article,discount);
    return article;
  });
}

module.exports = {
  applyPercent,
  applyAmount,
  applyDiscounts
};


function main() {
  fs.readFile('./input.json',{encoding:'utf-8',flag:'r'}, (err,data) => {
    if (err) throw err;
    input = JSON.parse(data);

    input.articles = applyDiscounts(input.articles,input.discounts);
    
    lvl1_output = lvl1_index.getResumeCards(input.carts,input.articles);

    lvl2_output = lvl2_index.forgeResumeCartsWithDeliveryFees(lvl1_output,input.delivery_fees);

    fs.writeFile('./output.json', JSON.stringify(lvl2_output), (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
  });
}

if (require.main === module) {
  main();
}