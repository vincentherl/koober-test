PROJECT = "Koober"

install=npm install --loglevel=error

test=npm test

run=node index.js

all: install test run

install: ;
	cd level1/ && $(install)
	cd level2/ && $(install)
	cd level3/ && $(install)

test: ;
	cd level1/ && $(test)
	cd level2/ && $(test)
	cd level3/ && $(test)

run: ;
	cd level1/ && $(run)
	cd level2/ && $(run)
	cd level3/ && $(run)