const lvl1_index = require('../level1/index.js');
const fs = require('fs');

/**
 * @param carts
 * @param deliveriesFees
 * @returns {Uint8Array | any[] | Int32Array | Uint16Array | Uint32Array | Float64Array | any}
 */
function forgeResumeCartsWithDeliveryFees(carts,deliveriesFees) {
  return carts.map((cart) => {
    cart.total += getDeliveryFees(cart.total,deliveriesFees);
    return cart;
  });
}

/**
 * @param totalChargeCart
 * @param deliveriesFees
 * @returns {number}
 */
function getDeliveryFees(totalChargeCart,deliveriesFees) {
  for (var deliveryFees of deliveriesFees) {
    if (
      totalChargeCart < deliveryFees.eligible_transaction_volume.max_price
      &&
      totalChargeCart >= deliveryFees.eligible_transaction_volume.min_price
    ) {
      return deliveryFees.price;
    }
  }
  return 0;
}

function main() {
  fs.readFile('./input.json',{encoding:'utf-8',flag:'r'}, (err,data) => {
    if (err) throw err;
    input = JSON.parse(data);

    lvl1_output = lvl1_index.getResumeCards(input.carts,input.articles);

    output = forgeResumeCartsWithDeliveryFees(lvl1_output,input.delivery_fees);

    fs.writeFile('./output.json', JSON.stringify(output), (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
  });
}

module.exports = {
  getDeliveryFees:getDeliveryFees,
  forgeResumeCartsWithDeliveryFees:forgeResumeCartsWithDeliveryFees
};

if (require.main === module) {
  main();
}