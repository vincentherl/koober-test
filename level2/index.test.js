const index = require('./index.js')

test('I should get delivery fees', () => {
  var delivery_fees = [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ];
  
  expect(index.getDeliveryFees(3000,delivery_fees)).toBe(0);
  expect(index.getDeliveryFees(0,delivery_fees)).toBe(800);
});

test('I should get resume carts with delivery fees', () => {
  var inputResumeCarts = [
    {
      "id": 1,
      "total": 2000
    },
    {
      "id": 2,
      "total": 1400
    },
    {
      "id": 3,
      "total": 0
    }
  ];

  var delivery_fees = [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ];
  
  var output = [
    {
      "id": 1,
      "total": 2000
    },
    {
      "id": 2,
      "total": 1800
    },
    {
      "id": 3,
      "total": 800
    }
  ];
  
  expect(index.forgeResumeCartsWithDeliveryFees(inputResumeCarts,delivery_fees)).toEqual(output);
  
});