# DONE

## Install & Test

### Shortcut makefile on the root

If **make** is present do just ```make``` or ( ```make install``` ```make test``` ```make run``` are available too)

###  Otherwise ...

In each level folder you can install with ```npm install``` run test with ```npm test``` and run with ```node index.js```

## Warning

- Functions are reused between lvl.
- The globally data pipeline of each lvl are not tested and are in the main function.
- The test coverage is minimal, the edge case are not tested.


# Koober Tech Interview

## Guidelines

- [duplicate](https://help.github.com/articles/duplicating-a-repository/) this repository (do **not** fork it)
- solve the levels in ascending order
- commit often to show the evolution of the code


## What we expect

- code should be clean (ES6, Typescript, Flow allowed)
- tests should be present (Show your best skills : TDD, unit, integration and more)
- synchronous IO should not be used (ex: use fs.readdir not fs.readdirSync)
- it must run in a MacOS or Linux environment, npm installed and node v8.0.0 or more recent
- comments could be added when you need to clarify a design decision or assumptions about the spec
- documentation could be added to clarify any details (how to run tests, etc)

## Acknowledgements

This test is shamelessly inspired by the Auchan:Direct interview, which was inspired by the Drivy interview :-) .
